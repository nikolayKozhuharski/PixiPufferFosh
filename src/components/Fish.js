import { Container, Graphics, Sprite } from "pixi.js";
import gsap from "gsap";

export default class Fish extends Sprite {
  constructor() {
    super();
    this.name = "fish";
    this.interactive = true;
    this.buttonMode = true;
    let isSmall = true;
    this.bigFish = Sprite.from("big");
    this.smallFish = Sprite.from("small");
    this.contract();
    this.init(isSmall);
  }

  expand() {
    this.removeChild(this.smallFish);
    this.addChild(this.bigFish);
  }
  contract() {
    this.removeChild(this.bigFish);
    this.addChild(this.smallFish);
  }

  init(isSmall) {
    this.on("pointerup", () => {
      isSmall = !isSmall;
      if (isSmall) {
        this.contract();
      } else {
        this.expand();
      }
    });
  }
}
